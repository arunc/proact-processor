 PROACT - Processor for On Demand Approximate Computing
------------------------------------------------------------------------------
## Arun Chandrasekharan < arun@uni-bremen.de >

## Group of Computer Architecture, (AGRA)
## Dept. of Computer Science - University of Bremen, Germany.
## http://www.informatik.uni-bremen.de/agra/eng/index.php
------------------------------------------------------------------------------
## License : GPL-V3
------------------------------------------------------------------------------

Proact ( **Pro**cessor for **A**proximate **C**ompu**t**ing ) is  based on 
Rocket Chip (RISCV ISA).


FPGA Zedboard implmentation is a separate repository : git@gitlab.com:arunc/proact-zedboard.git 
 
Application development targeted for Proact is another repository : git@gitlab.com:arunc/proact-apps.git

################################################################################

How to build various tools :: ./build/HowToBuild.txt

./docs  :: Various documents 

./emulator :: RISCV Emulator (after building as given in ./build/HowToBuild.txt)

./bin   :: RISCV Tools 
	(Cross compiler (GCC), Instruction set simulator (Spike), Proxy Kernel (PK))

./ext   :: Extra stuffs related to this project.

./utils :: Other utilities/general scripts.

################################################################################

Note: 

Cloned the original Rocket-Chip repository on Mai 11, 2016, and since then
the original projects are not tracked. All the sub-repositories/separate projects 
(about 15-20 of them) are merged to create a easily workable single repository.

Corresponding git version of the sub repositories can be obtained from docs/git-info.txt


################################################################################
