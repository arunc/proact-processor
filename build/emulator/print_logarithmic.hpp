// PRINT_LOGARITHMIC_HPP_BEGINS_HERE
//
// Copyright (C) AGRA - University of Bremen
//
// LICENSE : GNU GPLv3
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : print_logarithmic.hpp
// @brief  : Prints only intermittent log lines.
//           i.e 
//           first 5000 instructions from the emulator are printed as it is.
//           then every 100th instruction is printed x 100 times.
//           then every 1000th instruction is printed x 100 times.
//           then every 10000th instruction is printed x 100 times.. and so on.
//           if the pc address is within main_return_address +/- 500, these
//                 instructions are also printed irrespective of the above.
//           Once the main_return_address is crossed, then next 5000 instructions
//                 are printed. May exit before that due to other signals internal
//                 to the emulator.
//
//
//           All these are default values and the program takes values from 
//           environment variables 
//------------------------------------------------------------------------------


#ifndef PRINT_LOGARITHMIC_HPP
#define PRINT_LOGARITHMIC_HPP


#include <string>
#include <iostream>
#include <cstdlib>
#include <cassert>
#include <boost/regex.hpp>
#include <boost/lexical_cast.hpp>

namespace {
  auto is_initialized = false;
  auto INIT_INSTR_COUNT = 5200u;
  auto FINAL_INSTR_COUNT = 5000u;
  auto MAIN_RETURN_ADDRESS = 10000u;
  auto ADDRESS_BEFORE_RETURN = 9500u;
  auto LOGARITHMIC_PRINT = 1u;


  auto end_of_program = false;
  auto count_after_main_return = 0u;
  auto instr_count = 0u;
  auto print_count = 0u;
  auto print_multiplier = 10u;

  // Compile at the beginning itself.
  static const boost::regex  count_regexp  ("^C\\d:\\s+(\\d+)\\s\\[\\d\\]\\spc=\\[.*");
  static const boost::regex  pc_regexp  ("^C\\d:\\s+\\d+\\s\\[\\d\\]\\spc=\\[(.*)\\]\\sW\\[.*\\]\\sR\\[.*\\]\\sR\\[.*\\]\\sinst=.*");

  unsigned get_env_var ( const std::string &var, const unsigned &default_val) {
    const char * val = std::getenv( var.c_str() );
    if ( val == 0 ) 
      return default_val;
    else 
      return boost::lexical_cast <unsigned> (val);
    return 0u;
  }

  void setup_env_constraints () {
    if (is_initialized) return;
    is_initialized = true;

    LOGARITHMIC_PRINT = get_env_var ("LOGARITHMIC_PRINT", LOGARITHMIC_PRINT);
    if (0u == LOGARITHMIC_PRINT) return; // turned off, no need to evaluate anything.
    assert ( (1u == LOGARITHMIC_PRINT) || (0u == LOGARITHMIC_PRINT)  && 
	     "LOGARITHMIC_PRINT env variable must be 1 (ON) or 0 (OFF). If unspecified 1 (ON) ");
    

    INIT_INSTR_COUNT = get_env_var ( "INIT_INSTR_COUNT", INIT_INSTR_COUNT );
    FINAL_INSTR_COUNT = get_env_var ( "FINAL_INSTR_COUNT", FINAL_INSTR_COUNT );
    MAIN_RETURN_ADDRESS = get_env_var ("MAIN_RETURN_ADDRESS", MAIN_RETURN_ADDRESS);
    ADDRESS_BEFORE_RETURN = get_env_var ("ADDRESS_BEFORE_RETURN", ADDRESS_BEFORE_RETURN);
    
    std::cout << "Info: INIT_INSTR_COUNT = "    << INIT_INSTR_COUNT 
	      << "      FINAL_INSTR_COUNT = "   << FINAL_INSTR_COUNT 
	      << "      MAIN_RETURN_ADDRESS = " << MAIN_RETURN_ADDRESS
	      << "      ADDRESS_BEFORE_RETURN = " << ADDRESS_BEFORE_RETURN << std::endl;
  }


  int get_cycle_count (const char char_str[]) {
    boost::cmatch counts;
    if (boost::regex_match (char_str, counts, count_regexp) ) {
      return boost::lexical_cast <int> (counts[1]);
    }
    else {
      return -1;
    }
  }


  unsigned long long get_pc (const char char_str[]) {
    boost::cmatch pcs;
    if (boost::regex_match (char_str, pcs, pc_regexp) ) {
      std::string pc_str ( "0x" + pcs[1] );
      std::cout << pc_str << std::endl;
      return std::stoull (pc_str, nullptr, 16);
    }
    else {
      return 0ull;
    }
  }

  
  bool line_to_print (const char char_str[]) {
    setup_env_constraints ();
    // all special cases.
    // 1. if end of main reached, print FINAL_INSTR_COUNT number of instr
    if (end_of_program) {
      // exit the emulator in this case.
      if (count_after_main_return == FINAL_INSTR_COUNT) std::exit(9);
      count_after_main_return++;
      return true;
    }
    auto count = get_cycle_count (char_str);
    // 2. if the string is something else, e.g. from memory, print directly.
    if (-1 == count) return true; 
    // 3. if the instructions are in initial print range, return true.
    assert (count >= 0u && "Instruction count a negative number!"); 
    if (count < INIT_INSTR_COUNT) return true;
    // 4. if the instruction reached return address, return true.
    // now need to get the PC address.
    auto pc = get_pc (char_str);
    if (pc == MAIN_RETURN_ADDRESS) {
      end_of_program = true;
      return true;
    }
    // 5. If the pc is within range then return true.
    if ( pc >= ADDRESS_BEFORE_RETURN && pc <= MAIN_RETURN_ADDRESS ) return true;
    
    // 6. None of these -> start log printing.
    if (instr_count >= print_multiplier) {
      print_count++;
      instr_count = 0u;
      return true;
    }
    if (print_count >= print_multiplier) { // 100 x 100th instr, 1000 x 1000th instr
      print_multiplier = print_multiplier * 10u;
      print_count = 0u;
      return true;
    }
    instr_count++;
    return false;
  }


}




template <int w, typename... Args>
static ssize_t dat_log_fprintf(FILE *f, const char* fmt, Args... args)
{
  // Be generous. It doesn't really cost us anything.
  char str[w*2+1];
  dat_format(str, fmt, args...);
  ssize_t len = strlen(str);
  assert(len < sizeof(str));

  std::cout << "Called log \n" << std::endl;
  char ssstr [] = "In dat_log_fprintf ";
  fwrite(ssstr, 1, strlen(ssstr), f); 
  if ( line_to_print (str) ) return fwrite(str, 1, len, f); 
  return 0;
}

template <int w, typename... Args>
static ssize_t dat_log_prints(std::ostream& s, const char* fmt, Args... args)
{
  // This failed silently trying to print Flo's when the size was w/8+1.
  // Be generous. It doesn't really cost us anything.
  char str[w*2+1];
  dat_format(str, fmt, args...);
  ssize_t len = strlen(str);
  assert(len < sizeof(str));

  char ssstr [] = "In dat_log_prints ";
  s.write (ssstr, strlen(ssstr));

  if ( line_to_print (str) ) 
    s.write(str, len);
  else 
    return -1;
  ssize_t ret = s.good() ? len : -1;
  return ret;
}


//------------------------------------------------------------------------------
#endif
//------------------------------------------------------------------------------
