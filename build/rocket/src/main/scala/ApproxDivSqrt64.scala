// ApproxDivSqrt64 - Floating point double precision approximate divider.
// principle - compute once, use later.

package rocket

import Chisel._
import hardfloat._
import consts._


class ApproxDivSqrt64 extends Module {
  val io = new hardfloat.DivSqrtRecF64_IO;

  // some stuffs for convenience.
  val zero = Bits(0);     val one = Bits(1);  
  val True = Bool(true);  val False = Bool(false);

  val entries = 128;
  val tagWidth = io.out.getWidth - 1; // tag is the IEEE-754 DP format (64 bits)
  // result is Berkeley recoded DP format (65 bits) + exceptions (5 bits)
  val resultWidth = io.out.getWidth + io.exceptionFlags.getWidth ;  
  val nMaskBits = io.nMaskBits;
  val expApproxLevel = Cat (io.expSuperApproxEnable, io.expApproxEnable);
  val appxcore = new AssocMemory (entries, tagWidth, resultWidth);
  val divsqrt = Module (new hardfloat.DivSqrtRecF64);

  // note0: Add the approximation bypass signal if needed here.
  //io <> appxcore.io.fromCore;
  //appxcore.io.toFDiv <> divsqrt.io;

  // state signals.
  val s_idle :: s_startDiv :: s_startAppx :: s_updateAppx :: s_startFPUDiv :: s_startFPUSqrt :: Nil = Enum (UInt(), 6);
  val state = Reg (init = s_idle);
  // signals to store the inputs.
  val aWidth = io.a.getWidth;
  val rmWidth = io.roundingMode.getWidth
  val aReg = Reg (init = UInt (0, aWidth));
  val bReg = Reg (init = UInt (0, aWidth));
  val rmReg = Reg (init = UInt (0, rmWidth));
  // mantissa part.
  val aMant = Cat (aReg(64), aReg(51, 0)) ; // 64th bit is the sign. in re-coded format.
  val bMant = Cat (bReg(64), bReg(51, 0)) ; // 64th bit is the sign. in re-coded format.

  // to track AssocMemory address.
  val addrWidth = log2Up(entries) + 1;
  val memaddr = Reg ( init = UInt (0, width = addrWidth ));

  val inReady_sqrt = Reg (init = False);
  val inReady_div  = Reg (init = False);

  //conversion from recoded format to standard IEEE-754 DP.
  val proper_out = hardfloat.fNFromRecFN (11, 53, divsqrt.io.out);
  val proper_a   = hardfloat.fNFromRecFN (11, 53, aReg);
  val proper_b   = hardfloat.fNFromRecFN (11, 53, bReg);

  val isApproxValid = (appxcore.areTagsInTable (proper_a, proper_b, nMaskBits) & io.approxEnable);
  val tagaddr = appxcore.getTagAddress (proper_a, proper_b, nMaskBits);

  // for exponent approximations.
  val exp_a = proper_a(62, 52);
  val exp_b = proper_b(62, 52);
  val a_gt_b = Mux (exp_a > exp_b, exp_a -exp_b, UInt(0) );
  val a_lt_b = Mux (exp_a < exp_b, exp_b -exp_a, UInt(0) );

  val a_gt_b_1 = (a_gt_b > UInt(100)) & (expApproxLevel >= UInt(1)) & io.approxEnable;
  val a_gt_b_2 = (a_gt_b > UInt(50))  & (expApproxLevel >= UInt(2)) & io.approxEnable;
  val a_gt_b_3 = (a_gt_b > UInt(10))  & (expApproxLevel >= UInt(3)) & io.approxEnable;
  val all_a_gt_b = a_gt_b_1 | a_gt_b_2 | a_gt_b_3;

  val a_lt_b_1 = (a_lt_b > UInt(100)) & (expApproxLevel >= UInt(1)) & io.approxEnable;
  val a_lt_b_2 = (a_lt_b > UInt(50))  & (expApproxLevel >= UInt(2)) & io.approxEnable;
  val a_lt_b_3 = (a_lt_b > UInt(10))  & (expApproxLevel >= UInt(3)) & io.approxEnable;

  val a_gt_lt_b = all_a_gt_b | a_lt_b_1 | a_lt_b_2 | a_lt_b_3;

  //----------------------------------------------------------------------------
  // FSM
  when (state === s_idle) {
    inReady_sqrt := True; inReady_div := True;
    when (io.inValid) {
      aReg := io.a; bReg := io.b; rmReg := io.roundingMode; // store the inputs.
      when (io.sqrtOp) {state := s_startFPUSqrt}
      .otherwise {state := s_startDiv}
      inReady_sqrt := False; inReady_div := False;

      printf ("[i] FPUDivSqrt state = s_idle\n");

    }
  }
  when (state === s_startFPUSqrt) { // start FPUSqrt and wait until the sqrt finishes
    when (divsqrt.io.outValid_sqrt) {state := s_idle}
    inReady_sqrt := False; inReady_div := False;

    printf ("[i] FPUDivSqrt state = s_startFPUSqrt\n");
    printf ("[w] FPU hardware SquareRoot invoked - may not be stable.\n")

  }
  when (state === s_startDiv) { // decide approx or FPUDiv or wait until FPUDiv is ready.
    when (a_gt_lt_b) { state := s_idle;} // skip everything, direct approximate and goto idle.
    .elsewhen (isApproxValid) {state := s_startAppx;}
    .otherwise { when (divsqrt.io.inReady_div) { state := s_startFPUDiv; } }
    inReady_sqrt := False; inReady_div := False;

    printf ("[i] FPUDivSqrt state = s_startDiv\n");
    
  }
  when (state === s_startAppx) { // feed the output and go to idle
    state := s_idle
    inReady_sqrt := False; inReady_div := False;

    printf ("[i] FPUDivSqrt state = s_startAppx\n");
    printf ("[i] FPUDivSqrt state = s_startAppx with result = 0x%x exception = %d (a = 0x%x   b = 0x%x  rm = %d) (tag = %d \n",
      hardfloat.fNFromRecFN (11, 53, io.out), io.exceptionFlags, 
      proper_a, proper_b, rmReg, tagaddr );

  } 
  when (state === s_startFPUDiv)  { // start FPUDiv and wait until the div finishes.
    when (divsqrt.io.outValid_div) {
      state := s_idle;
      when (io.approxEnable === True) {
        appxcore.updateTagsAndResult ( memaddr, proper_a, proper_b,
          Cat (divsqrt.io.exceptionFlags, divsqrt.io.out) ) ; // update to appxcore.

        printf ("[i] FPUDivSqrt state = s_startFPUDiv with ApproxCore update. appxcore addr = %d value = 0x%x \n",
          memaddr, Cat (divsqrt.io.exceptionFlags, divsqrt.io.out) );
        printf ("[i] FPUDivSqrt state = s_startFPUDiv with result = 0x%x exception = %d (a = 0x%x  b = 0x%x  rm = %d)\n",
          proper_out, divsqrt.io.exceptionFlags, proper_a, proper_b, rmReg);

      }
    }
    inReady_sqrt := False; inReady_div := False;

    printf ("[i] FPUDivSqrt state = s_startFPUDiv \n");

  }
  //----------------------------------------------------------------------------
  // Update the appxcore address, if fpudiv is in use and appxcore is updated.
  when ((state === s_startFPUDiv) & divsqrt.io.outValid_div ) {
    memaddr := (memaddr + UInt(1, addrWidth ))  % UInt (entries, addrWidth);
  }
  //----------------------------------------------------------------------------
  // combo signals.

  // divsqrt : data signals are common to every state for divsqrt.
  divsqrt.io.a := aReg; divsqrt.io.b := bReg; divsqrt.io.roundingMode := rmReg;

  // divsqrt : input control signals. 
  val div_inValid = divsqrt.io.inReady_div & (isApproxValid === zero);
  when ((state === s_startDiv) & div_inValid) {divsqrt.io.inValid := one}
  .elsewhen ((state === s_startFPUSqrt) & divsqrt.io.inReady_sqrt) {divsqrt.io.inValid := one}
  .otherwise {divsqrt.io.inValid := zero}
  divsqrt.io.sqrtOp  := (state === s_startFPUSqrt);

  // top : outputs.
  
  when ( (state === s_startDiv) & a_gt_lt_b ) {
    val mantissa = aReg(51, 0); // The result is always aMantissa in recoded-format.
    val sign = Mux (aReg(64) === bReg(64), UInt(0), UInt(1)); // recoded-format. hence aReg(64)
    val expBias = UInt (1023, width = 11);
    //---// (a > b) => 1023 + (a - b)
    //---// (a < b) => 1023 - (b - a)
    val exponent1 = expBias + ( aReg(63, 52) - bReg(63, 52) );
    val exponent2 = expBias - ( bReg(63, 52) - aReg(63, 52) );

    when ( (exp_a >= expBias)  && (exp_b >= expBias) ) {
      when (exp_a >= exp_b) { io.out := Cat (sign, exponent1, mantissa); }
        .otherwise { io.out := Cat (sign, exponent2, mantissa); }
    }
    .elsewhen ( (exp_a < expBias) && (exp_b < expBias) ) {
      when (exp_a >= exp_b) { io.out := Cat (sign, exponent1, mantissa); }
        .otherwise { io.out := Cat (sign, exponent2, mantissa); }
    }
    .elsewhen ( (exp_a < expBias) && (exp_b >= expBias) ) {
      io.out := Cat (sign, exponent2, mantissa);
    }
    .otherwise {
      io.out := Cat (sign, exponent1, mantissa);
    }

    //io.out := Cat (sign, exponent, mantissa);
    //io.out := zero;
    io.exceptionFlags := zero;
    io.outValid_div := one; io.outValid_sqrt := zero;

    printf ("[i] FPUDivSqrt state = s_startDiv with exponent approximation. exp_a = %d exp_b = %d  exp_result1 = %d  exp_result2 = %d   [***expBias = %d] \n",
      exp_a, exp_b, exponent1 , exponent2, expBias
    );
  }
  .elsewhen ( (state === s_startFPUDiv) | (state === s_startFPUSqrt) ) { 
    io.out := divsqrt.io.out; io.exceptionFlags := divsqrt.io.exceptionFlags;
    io.outValid_div := divsqrt.io.outValid_div; io.outValid_sqrt := divsqrt.io.outValid_sqrt;
  }
  .elsewhen (state === s_startAppx) {
    val appxresult = appxcore.getResult (tagaddr );

    printf ("[i] FPUDivSqrt state = s_startAppx with ApproxCore retrieval. appxcore addr = %d value = 0x%x \n",
      tagaddr, appxresult );

    io.out := appxresult (resultWidth - io.exceptionFlags.getWidth - 1, 0);
    io.exceptionFlags := appxresult (resultWidth - 1, resultWidth - io.exceptionFlags.getWidth);
    io.outValid_div := one; io.outValid_sqrt := zero;
  }
  .otherwise { // nothing valid.
    io.out := zero; io.exceptionFlags := zero; 
    io.outValid_div := zero; io.outValid_sqrt := zero;
  }

  io.inReady_sqrt := inReady_sqrt; // asserted in strict sequence (s_idle only). 
  io.inReady_div  := inReady_div;  // asserted in strict sequence (s_idle only).
  //----------------------------------------------------------------------------
  // note1: FPU sqrt - this has to be thoroughly cross-checked.
  // for the timebeing, check if something like assert can be given.
  //
  // note2: inReady and inValid need to be asserted in the SAME clock cycle.
  // its not one is sampled first, checked and in the next cycle the other is asserted.
  //
  // note3: extra latency.
  // The statemachine works in strict sequence, disregarding the pipeline.
  // For our experiments with some 50 random numbers there is a performance hit of 16% 
  // i.e. cycles increased by 16%. 6554 cycles vs 5639 cycles.
  // Must optimize this, else there is no need of any approximation!

}

