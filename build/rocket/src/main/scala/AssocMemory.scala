//
// This is a double tagged associative memory. (well not a strict 'associative memory'.)
//
// Working : stores 2 tags and 1 result.
//           Both the tags have to match to get the result.
//           Address to which tags and result are stored can be generated externally
//           or implicitly updated (using getNextAddress()).
//           However using getNextAddress() should not be used for now, since I don't
//           know what the circuit is generated. It might take another clk cycle extra.
//

package rocket

import Chisel._
import Util._



class AssocMemory (entries:Int, tagWidth:Int, resultWidth:Int) {
  val tagTable = Mem (entries, UInt (width = tagWidth*2) );
  val resultsTable  = Mem (entries, UInt (width = resultWidth) ); 

  val addrWidth = log2Up(entries) + 1;
  val memaddr = Reg ( init = UInt (0, width = addrWidth ));

  val fullMask = Fill (tagWidth, Bool(true));

  def areTagsInTable ( tag1:UInt, tag2:UInt,  nMaskBits:UInt ) = {
    val mm = (fullMask >> nMaskBits) << nMaskBits;
    val mmm = mm(tag1.getWidth - 1, 0);
    val mask = Cat (mmm, mmm);
    val tt = Cat (tag1, tag2) & mask;
    val tableMatch = tagTable.map (_ & mask).map (_ === tt).toBits.orR;
    tableMatch
  }

  def getTagAddress ( tag1:UInt, tag2:UInt, nMaskBits:UInt ) = {
    val mm = (fullMask >> nMaskBits) << nMaskBits;
    val mmm = mm(tag1.getWidth - 1, 0);
    val mask = Cat (mmm, mmm);
    val tt = Cat (tag1, tag2) & mask;
    val table = tagTable.map (_ & mask).map (_ === tt);
    val addr = Vec (table).indexWhere ( (x:Bool) => x === Bool(true) )
    addr
  }

  def getResult (addr: UInt) = { resultsTable(addr)  }

  def updateTagsAndResult (addr:UInt, tag1:UInt, tag2:UInt, result:UInt) = {
    tagTable(addr) := Cat (tag1, tag2);
    resultsTable(addr)  := result;
  }

  // automatically calculate the next address and update the table
  // But careful, calculating next address may consume 1 clk cycle (is this correct???).
  // Hence this is a 2 cycle operation.
  def updateTagsAndResult (tag1:UInt, tag2:UInt, result:UInt) = {
    printf ("[Warning] updateTagsResult(tag1:UInt, tag2:UInt, result:UInt) spans 2 clk cycles.\n");
    val addr = getNextAddress ();
    tagTable(addr) := Cat (tag1, tag2);
    resultsTable(addr)  := result;
  }

  def getNextAddress () = { 
    memaddr := (memaddr + UInt(1, addrWidth ))  % UInt (entries, addrWidth);
    memaddr
  }

  def getCurrentAddress () = { memaddr } // mostly useless and stupid.

}


 
