#!/usr/bin/env bash
# @brief   : Setup the environment necessary for Rocket Chip.
#
#-------------------------------------------------------------------------------

pushd `dirname $0` > /dev/null
SCRPATH=`pwd`
popd > /dev/null


# These variables take override/preference over the previously set values.
export RISCV=${SCRPATH}/../bin
export PROACT=${RISCV}
unset  ROCKETCHIP
export PATH=${RISCV}/bin:${PATH}


printf "RISCV (PROACT) = ${RISCV} \n"





