//
// Display some samples of floating point berkely and IEEE-754 
// and the split up of sign, exp and frac parts.
// Read docs/notes.txt for further details.
//
// For the Berkeley internal format, ONLY the hex value displayed
// makes sense.

// All the numbers obtained are converted from actual run samples.

#include <boost/dynamic_bitset.hpp>
#include <string>
#include <iostream>
#include <sstream>


namespace {
  auto spc = ",";
}
unsigned long long to_ulong (std::string val, std::string hx) {
  unsigned long long value;
  std::istringstream iss (hx);
  iss >> std::hex >> value;
  std::cout << val << spc << hx << spc;
  return value;
}

void print_float (unsigned long long val) {
  boost::dynamic_bitset <> bs (64, val);
  auto sign = bs[63];
  
  boost::dynamic_bitset <> exp  (64, 0);
  boost::dynamic_bitset <> frac (64, 0);

  auto j = 0u;
  for (auto i=52u; i <= 62u; i++) {
    if (bs.test(i)) exp[j] = 1;
    j++;
  }

  j = 0u;
  for (auto i=0u; i <= 51u; i++) {
    if (bs.test(i)) frac[j] = 1;
    j++;
  }

  std::stringstream expss, fracss;
  expss << std::hex  << std::uppercase << exp.to_ulong();
  fracss << std::hex  << std::uppercase << frac.to_ulong();
  std::cout << sign << spc
	    << "( "<< expss.str()  << spc << exp.to_ulong()  << " )" << spc
	    << "( "<< fracss.str() << spc << frac.to_ulong() << " )" 
	    << std::endl;
}

void print_all (std::string val, std::string hx) {
  auto num = to_ulong (val, hx);
  print_float (num);
}


int main () {
  std::string val, hx;
  std::cout << "Recoded" << std::endl;
  std::cout << "Other than hex, rest is WRONG" << std::endl;

  std::cout << "Value" << spc << "Hex" << spc << "Sign" << spc 
	    << "( Exp" << spc << "Exp )"  << spc
	    << "( Frac" << spc << "Frac )" << std::endl;



  val ="0.50";   hx = "0x07ff0000000000000";  print_all (val, hx);
  val ="0.66";   hx = "0x07ff5555555555555";  print_all (val, hx);
  val ="0.75";   hx = "0x07ff8000000000000";  print_all (val, hx);
  val ="1.33";   hx = "0x08005555555555555";  print_all (val, hx);
  val ="-1.33";  hx = "0x18005555555555555";  print_all (val, hx);
  val ="0.5";    hx = "0x07ff0000000000000";  print_all (val, hx);
  val ="0";      hx = "0x11000000000000000";  print_all (val, hx);
  val ="-0";     hx = "0x103d0000000000000";  print_all (val, hx);
  val ="inf";    hx = "0x0c286b50000000731";  print_all (val, hx);
  val ="-inf";   hx = "0x1d505be0000000731";  print_all (val, hx);
  val ="ind";    hx = "0x0f508000000000000";  print_all (val, hx);
  val ="1";      hx = "0x08000000000000000";  print_all (val, hx);
  val ="2";  hx = "0x08010000000000000";  print_all (val, hx);
  val ="3";  hx = "0x08018000000000000";  print_all (val, hx);
  val ="4";  hx = "0x08020000000000000";  print_all (val, hx);
  val ="-4";  hx = "0x18020000000000000";  print_all (val, hx);
  val ="-1";  hx = "0x18000000000000000";  print_all (val, hx);
  val ="0";  hx = "0x01c20000000000000";  print_all (val, hx);
  val ="-0";  hx = "0x01c20000000000000";  print_all (val, hx);
  val ="2";  hx = "0x08010000000000000";  print_all (val, hx);
  val ="-2";  hx = "0x18010000000000000";  print_all (val, hx);
  val ="0";  hx = "0x01c20000000000000";  print_all (val, hx);
  val ="2";  hx = "0x08010000000000000";  print_all (val, hx);
  val ="3";  hx = "0x08018000000000000";  print_all (val, hx);
  val ="4";  hx = "0x08020000000000000";  print_all (val, hx);
  val ="3";  hx = "0x08018000000000000";  print_all (val, hx);
  val ="3";  hx = "0x08018000000000000";  print_all (val, hx);
  val ="-2";  hx = "0x18010000000000000";  print_all (val, hx);
  val ="-2";  hx = "0x18010000000000000";  print_all (val, hx);
  val ="-2";  hx = "0x18010000000000000";  print_all (val, hx);
  val ="0";  hx = "0x01c20000000000000";  print_all (val, hx);
  val ="0";  hx = "0x01c20000000000000";  print_all (val, hx);
  val ="0";  hx = "0x01c20000000000000";  print_all (val, hx);


  std::cout << spc << spc << spc << spc << spc << spc << std::endl;
  std::cout << spc << spc << spc << spc << spc << spc << std::endl;
  std::cout << "IEE754-D" << std::endl;

  std::cout << "Value" << spc << "Hex" << spc << "Sign" << spc 
	    << "( Exp" << spc << "Exp )"  << spc
	    << "( Frac" << spc << "Frac )" << std::endl;

  std::cout << spc << spc << spc << spc << spc << spc << std::endl;

  val = "0.5"; hx = "0x3fe0000000000000";  print_all (val, hx);
  val = "1";   hx = "0x3ff0000000000000";  print_all (val, hx);
  val = "2";   hx = "0x4000000000000000";  print_all (val, hx); 
  
  val = "nan"; hx = "0x7ff8000000000000";  print_all (val, hx);
  val =" 0 ";  hx = "0x0000000000000000  ";  print_all (val, hx);
  val = " 0 "; hx = "0x0000000000000000";  print_all (val, hx);
  val = "inf"; hx = "0x7ff0000000000000";  print_all (val, hx);
  val = "2";   hx = "0x4000000000000000";  print_all (val, hx);
  val = " 0 "; hx = "0x0000000000000000";  print_all (val, hx);
  val = "0.43658183322987787"; hx = "0x3fdbcf0e75e8ccac";  print_all (val, hx);
  val = "4.22"; hx = "0x4010cccccccccccd";  print_all (val, hx);
  val = "9.666"; hx = "0x402354fdf3b645a2";  print_all (val, hx);
  val = "2.0";   hx = "0x4000000000000000";  print_all (val, hx);
  val = "5";     hx = "0x4014000000000000";  print_all (val, hx);
  val = "2.5";   hx = "0x4004000000000000";  print_all (val, hx);
  val = "-2.0";  hx = "0xc000000000000000";  print_all (val, hx);
  val = "-5";    hx = "0xc014000000000000";  print_all (val, hx);
  val = "2.5";   hx = "0x4004000000000000";  print_all (val, hx);
  val = "-2.0";  hx = "0xc000000000000000";  print_all (val, hx);
  val = "5";     hx = "0x4014000000000000";  print_all (val, hx);
  val = "-2.5";  hx = "0xc004000000000000";  print_all (val, hx);
  val = "0.5";   hx = "0x3fe0000000000000";  print_all (val, hx);
  val = "2.5";   hx = "0x4004000000000000";  print_all (val, hx);
  val = "5";     hx = "0x4014000000000000";  print_all (val, hx);



  return 0;

}
